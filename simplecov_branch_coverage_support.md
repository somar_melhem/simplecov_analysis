# Simplecove support branch cover metrices

### First analysis of the process: 

simplecov is using two gems to generate the report   

- simplecov original gem that responsible of creating result objects 
- simplecov-html respossible of generating the default template of report   

#### Coverage config section: 
first call here   
https://github.com/colszowka/simplecov/blob/master/lib/simplecov.rb#L53  
 
`Coverage.start` By default is handling only lines coverage, so to activate the branch coverage just need to pass like in the example below:  
``` 
Coverage.start(lines: true, branches: true) # note we can activate one of them but lines is the default  
```    

#### Coverage result calling   
  
it happens here inside the result method:   
https://github.com/colszowka/simplecov/blob/master/lib/simplecov.rb#L87 
 
Till now what issues we can face if we want to implement branch coverage report supporting   

1- Branch Coverage report is different from lines report [1] 
lines report   
```
{"*.rb"=>[1, nil, 1, 0, 1, 0, nil, 1, nil]}   
```
branches report   
```
{"cov.rb"=>{:branches=>{[:if, 0, 5, 0, 8, 10]=>{[:then, 1, 6, 2, 6, 10]=>0, [:else, 2, 8, 2, 8, 10]=>1}, [:if, 3, 3, 0, 9, 3]=>{[:then, 4, 4, 2, 4, 11]=>0, [:else, 5, 5, 0, 8, 10]=>1}}}} 
```
 
 
## Current work flow of simplecov report generating    
By calling `Simplecov.start &:block_of_config ` we call the start method that it handle config initializing and calling Coverage.start.  
on `at_exit` of this process we call Coverage.result and passing it to be analysed by simplecov result builder  
This builder is consider 3 facilities inside each other:    

    1- SimpleCov::Result, represent the main object that passed to formatter of simplecov with three main instance variables:  

                * original_result: original value of Coverage.result  
                * files: Array of SimpleCov::SourceFile objects  
                * command_name: name of command that called the SimpleCov service  
    2- SimpleCov::SourceFile represent the file that been covered, this class is full of methods that support the 
    simplecov-html library to generate it interface very easy 
    3- SimpleCov::Line represent each line in the file with support methods to handle a common calculations needed.    
We have some files that are not covered by tests, for this files SimpleCov have method `add_not_loaded_files` this method calls LinesClassifier on each file and do same what coverage.start for lines do, in other words, it give use the array with [0,nil] this result for linesClassifier also added to the result that we have.  
Result object passed to simplecov-html which handle generated related html depending on the result are given 

# Proposed solution for this issue   
1- On config or on calling the ```Coverage.start``` method we should handle the definition of ruby version and check if it supports branches measurement, maybe it can be implemented by writing method like : (this is just suggestion to discuss)  
```     
def start_coverage  
  if RUBY_VERSION.to_f < 2.5  
    Coverage.start  
  else  
    Coverage.start(:all) # or what ever passed in config like add_measurement_targets :branches, :methods
  end 
end  
```
2- We need to unit the result of the coverage, I mean called by old ruby or new ruby, or with and without options, it should always have same structure(Hash with measurement keys):  
```  
{  
lines: [1,0,4,nil],  
branches: {[:if, 1, 2.....]...},  
methods: {....}  
}  

```  
For this we need to create a SimpleCov::ResultAdapter handle to check if the result of coverage is Array or Hash and move it to hash with needed key ex: {:lines => [Array of 0, n, nil]}, this kind of solution help us escape repeatable logic of checking whether the result is Array or Hash at the next steps and also guarantee working of the old version of coverage on current changes, also this consider changes on LinesClassifier to give us hash result similar to above instead of array.

3- Not nessasary but prefered to create BranchesClassifier that will handle creating report similar or Coverage branches report for not loaded files.  

4- Create SimpleCov::SourceFile::Branch (similar to Line) will create an object branch which will have supprot methods that give needed statistics for simplecov-html or other formatters, NOTE-VIP: ``We need to wrapp relation between line and branch that we easy on spacific line check branche if exists and its relevant, keep in mind the lines array index start with 0 where in branchs its line 1``

5- On Simplecov-html side, we can modify the current views or create new views with that will be rendered depends on the resoult object that we have (we can pass to result @measuring_targets instance variable with boolean value that can handle define what to render)  
  
`Confused about some things like files looks like tests inside gem path `features/*` couldn't understand the format of them :/  `
About the estimation:   
1- ~4h  
2- ~8h   
3- ~24h       
4- ~24h
5- 8~10h depends on the solution that we can go with   
 
 
 
# Reference :   
### 1- line vs branche coverage 
Line Coverage values reference 
```  

0 - line is missed  

n - number of reads 

nil - empty line 
```  
Branche coverage value reference
```
[ BRANCH_TYPE, UNIQUE_ID, START_LINE_NUMBER, START_COLUMN_NUMBER, END_LINE_NUMBER, END_COLUMN_NUMBER ] 

[    :if,          0,            3,                  0,                7,             3          ]  
```